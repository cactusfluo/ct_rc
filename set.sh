#!/usr/bin/env bash

cat	.vimrc		>> ~/.vimrc
cat	.bashrc		>> ~/.bashrc
cp	.bash_alias	~/
cp	.bash_env	~/
